#FROM gitlab/gitlab-runner
FROM gitlab/gitlab-runner:alpine


RUN apk update
RUN apk upgrade

USER root

COPY config.toml ~/.gitlab-runner/
COPY config.toml /.gitlab-runner/
COPY config.toml /etc/gitlab-runner/

